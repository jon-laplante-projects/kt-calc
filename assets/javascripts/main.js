function calculate(rank){
	var top_daily_sales;
	var bottom_daily_sales;
	
	switch(true){
		case (rank >= 200000):
			top_daily_sales = 0;
			bottom_daily_sales = 0;
			break;
		case (rank >= 100000 && rank < 200000):
			top_daily_sales = 0.034;
			bottom_daily_sales = 0;
			break;
		case (rank >= 50000 && rank < 100000):
			top_daily_sales = 1;
			bottom_daily_sales = 0;
			break;
		case (rank >= 10000 && rank < 50000):
			top_daily_sales = 10;
			bottom_daily_sales = 5;
			break;
		case (rank >= 5500 && rank < 10000):
			top_daily_sales = 20;
			bottom_daily_sales = 15;
			break;
		case (rank >= 3000 && rank < 5500):
			top_daily_sales = 57;
			bottom_daily_sales = 25;
			break;
		case (rank >= 1500 && rank < 3000):
			top_daily_sales = 85;
			bottom_daily_sales = 70;
			break;
		case (rank >= 750 && rank < 1500):
			top_daily_sales = 110;
			bottom_daily_sales = 100;
			break;
		case (rank >= 500 && rank < 750):
			top_daily_sales = 145;
			bottom_daily_sales = 120;
			break;
		case (rank >= 350 && rank < 500):
			top_daily_sales = 200;
			bottom_daily_sales = 175;
			break;
		case (rank >= 200 && rank < 350):
			top_daily_sales = 370;
			bottom_daily_sales = 250;
			break;
		case (rank >= 35 && rank < 200):
			top_daily_sales = 1250;
			bottom_daily_sales = 500;
			break;
		case (rank >= 20 && rank < 35):
			top_daily_sales = 2500;
			bottom_daily_sales = 2000;
			break;
		case (rank >= 5 && rank < 20):
			top_daily_sales = 3500;
			bottom_daily_sales = 3000;
			break;
		case (rank >= 1 && rank < 5):
			top_daily_sales = 7500;
			bottom_daily_sales = 4000;
			break;
	}
	
	var daily_sales = {top: top_daily_sales, bottom: bottom_daily_sales};
	return daily_sales;
}

function calc_tasks(){
	if( $("#kindle_rank").val() !== null ) {
		// Clear any notifications.
		$("#notifications_div").html("");
		$("#notifications_div").hide();
		
		// Clear the current table data.
		$(".table").html("<tr><thead><th>Price</th><th>Daily Profit</th><th>% Profit</th></thead></tr>");
		
		var rank = $("#kindle_rank").val();
		
		rank = rank.replace(",", "");
		rank = rank.replace(".", "");
		rank = rank.replace("-", "");
		
		if ( isNaN(parseInt(rank)) !== true ){
			rank = parseInt(rank);	
		}

		if( typeof rank == "number" ) {
			var daily_sales = calculate(rank);

			$("#notifications_div").removeClass("alert-danger");
			$("#notifications_div").addClass("alert-info");
			if ( daily_sales["top"] > 0 ) {
				$("#notifications_div").html("<i class='fa fa-info-circle'></i>&nbsp;&nbsp;This book is selling between " + daily_sales["bottom"] + " and " + daily_sales["top"] + " copies a day.");

				var row_str;
			
				for( var x = 0.99; x < 10; x++ ){
					var row_start = "<tr><td>$" + x + "</td>";
					var row_close;
					
					if (x < 2.99){
						row_str = "<td>" + numeral( parseInt(x * parseInt(daily_sales["top"])) * 0.3 ).format('$0,0.00') + "</td>";
						row_close = "<td>30%</td></tr>";
					} else {
						row_str = "<td>" + numeral( parseInt(x * parseInt(daily_sales["top"])) * 0.7  ).format('$0,0.00') + "</td>";
						row_close = "<td>70%</td></tr>";
					}
					
					$(".table").append(row_start + row_str + row_close);
				}
				
				$(".table").slideDown("fast");
			} else {
				$(".table").slideUp("fast");
				
				$("#notifications_div").html("<i class='fa fa-info-circle'></i>&nbsp;&nbsp;This book is selling less than a copy a day.");
			};

			$("#notifications_div").slideDown("fast");
		} else {
			$("#notifications_div").removeClass("alert-info");
			$("#notifications_div").addClass("alert-danger");
			$("#notifications_div").html("<i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;This doesn't seem to be a Kindle store rank. Please make sure that you're entering a normal number, and try again.");
			$("#notifications_div").slideDown("fast");
			$(".table").slideUp("fast");
		}
	}
}

$(function(){
	var curr_date = new Date();
	var curr_year = curr_date.getFullYear();
	if (parseInt(curr_year) > 2015){
		curr_year = "2014 - " + curr_year;
	}
	$("#copyright_notice").html('&copy; ' + curr_year + ' Kindle Trend.<br/>');
	
	$("#kindle_rank").keyup(function(e){
		if (e.which == 13) {
			calc_tasks();
		}
	});

	$("#calc_btn").click(function(){
		calc_tasks();
	});
});